from flask_mvc_blog import app
from functools import wraps
from flask import g, request, redirect, url_for
from flask_mvc_blog.Controllers.articlesController import app_get_articles, app_register_article, app_handle_article, app_blog
from flask_mvc_blog.Controllers.usersController import app_get_users, app_handle_user, app_load_logged_in_user, app_register, app_login, app_logout, app_profile, app_home
from flask_mvc_blog.Controllers.commentsController import app_register_comment, app_handle_comment, app_get_comments

# Required decorateur


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if g.user is None:
            return redirect(url_for('login', next=request.url))
        return f(*args, **kwargs)
    return decorated_function

# App Routes
@app.route('/')
def index():
    return app_home()


@app.route('/profile')
@login_required
def profile():
    return app_profile()


@app.route('/blog')
def blog():
    return app_blog()


@app.before_request
def load_logged_in_user():
    return app_load_logged_in_user()


# Users Routes

@app.route("/register", methods=["GET", "POST"])
def register():
    return app_register()


@app.route('/login', methods=['GET', 'POST'])
def login():
    return app_login()


@app.route("/logout")
@login_required
def logout():
    return app_logout()


@app.route("/user/<int:id>", methods=["GET", "PUT", "DELETE"])
def handle_user(id):
    return app_handle_user(id)


@app.route("/users")
def get_users():
    return app_get_users()

# Articles Routes
@app.route('/articles/post', methods=["POST"])
def post_article():
    return app_register_article()


@app.route('/articles/id/<int:id>', methods=["GET", "PUT", "DELETE"])
def handle_article(id):
    return app_handle_article(id)


@app.route('/articles')
def get_articles():
    return app_get_articles()


# Comments Routes

@app.route('/comments', methods=["POST"])
def post_comment():
    return app_register_comment()


if __name__ == '__main__':
    app.run()
