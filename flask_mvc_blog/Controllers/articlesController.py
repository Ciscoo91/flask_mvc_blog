from flask import render_template, request, jsonify, g, flash, redirect, url_for, session
from flask_mvc_blog.Config.db import get_db, query_db
from werkzeug.security import check_password_hash, generate_password_hash


def app_get_articles():
    req_articles = query_db("select * from posts")

    if req_articles is not None:
        all_articles = []
        req_articles = list(req_articles)
        for req_article in req_articles:
            article = {}
            article["id"] = req_article["id"]
            article["author_id"] = req_article["author_id"]
            article["author"] = req_article["author"]
            article["created"] = req_article["created"]
            article["title"] = req_article["title"]
            article["body"] = req_article["body"]
            all_articles.append(article)
        # print(all_articles)
        return jsonify(all_articles)


def app_handle_article(id):
    user_id = session.get("user_id")  # Needed for update and delete

    if request.method == "GET":
        requested_article = query_db(
            "select * from posts where id = ?", [id], one=True)
        requested_comments = query_db(
            "select * from comments where post_id = ?", [id])

        if requested_article is not None:
            article = {}
            article["id"] = requested_article["id"]
            article["author_id"] = requested_article["author_id"]
            article["author"] = requested_article["author"]
            article["created"] = requested_article["created"]
            article["title"] = requested_article["title"]
            article["body"] = requested_article["body"]

            # return jsonify(article)
            return render_template("articles/article.html", article=article, comments=requested_comments)
    elif request.method == "PUT":
        art = query_db(
            "select * from posts where id = ?", [id], one=True)
        if user_id == art["author_id"]:
            title = request.form["title"]
            content = request.form["content"]
            article = query_db(
                "UPDATE articles SET title = ?, body = ? WHERE id = ?", [title, content, id])
            if article is not None:
                return "Article updated successfuly"
        else:
            return "you cannot update this article"

    elif request.method == "DELETE":
        art = query_db(
            "select * from posts where id = ?", [id], one=True)
        if user_id == art["author_id"]:
            article = query_db("DELETE FROM articles WHERE id = ?", [id])
            if article is not None:
                return "Article deleted successfuly"
        else:
            return "you cannot delete this article"


def app_register_article():
    error = None
    db = get_db()
    title = request.form["title"]
    content = request.form["content"]
    author = session.get("username")
    author_id = session.get("user_id")

    if not title:
        error = "Title is required."
    elif not content:
        error = "Content of the article is required"

    if error is None:
        db.execute("INSERT INTO posts(title, body, author, author_id) VALUES(?, ?, ?, ?)",
                   (title, content, author, author_id,))
        db.commit()
        return "Article posted successfully"
    # flash(error)
    # return render_template("index.html", error=error)
    else:
        return "An error occured while posting this article"


def app_blog():
    articles = query_db("select * from posts")

    if articles is not None:
        return render_template("articles/articles.html", articles=articles)
