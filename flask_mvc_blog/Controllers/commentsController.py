from flask import render_template, request, jsonify, g, flash, redirect, url_for, session
from flask_mvc_blog.Config.db import get_db, query_db
from werkzeug.security import check_password_hash, generate_password_hash


def app_get_comments():
    comments = query_db("select * from comments")

    if comments is not None:
        all_comments = []
        comments = list(comments)
        for req_comment in comments:
            comment = {}
            comment["id"] = req_comment["id"]
            comment["blog_id"] = req_comment["blog_id"]
            comment["author"] = req_comment["author"]
            comment["created"] = req_comment["created"]
            comment["content"] = req_comment["content"]
            all_comments.append(comment)
        # print(all_comments)
        return jsonify(all_comments)


def app_handle_comment(id):
    user_id = session.get("user_id")  # Needed for update and delete

    if request.method == "GET":
        requested_comment = query_db(
            "select * from comments where id = ?", [id], one=True)

        if requested_comment is not None:
            comment = {}
            comment["id"] = requested_comment["id"]
            comment["post_id"] = requested_comment["post_id"]
            comment["author"] = requested_comment["author"]
            comment["content"] = requested_comment["content"]
            comment["created"] = requested_comment["created"]

            return jsonify(comment)
    elif request.method == "PUT":
        art = query_db(
            "select * from posts where id = ?", [id], one=True)
        if user_id == art["author_id"]:
            title = request.form["title"]
            content = request.form["content"]
            article = query_db(
                "UPDATE articles SET title = ?, body = ? WHERE id = ?", [title, content, id])
            if article is not None:
                return "Article updated successfuly"
        else:
            return "you cannot update this article"

    elif request.method == "DELETE":
        art = query_db(
            "select * from posts where id = ?", [id], one=True)
        if user_id == art["author_id"]:
            article = query_db("DELETE FROM articles WHERE id = ?", [id])
            if article is not None:
                return "Article deleted successfuly"
        else:
            return "you cannot delete this article"


def app_register_comment():
    error = None
    db = get_db()
    content = request.form["comment"]
    author = session.get("username")
    author_id = session.get("user_id")
    post_id = request.args['post_id']
    if not content:
        error = "Content of the article is required"

    if error is None:
        db.execute("INSERT INTO comments(content, author, author_id, post_id) VALUES(?, ?, ?, ?)",
                   (content, author, author_id, post_id,))
        db.commit()
        return redirect(url_for('blog'))
    # flash(error)
    # return render_template("index.html", error=error)
    else:
        return "An error occured while posting this article"
