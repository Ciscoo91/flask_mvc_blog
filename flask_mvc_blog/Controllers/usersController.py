from flask import render_template, request, jsonify, g, flash, redirect, url_for, session
from flask_mvc_blog.Config.db import get_db
from werkzeug.security import check_password_hash, generate_password_hash


def app_home():
    return render_template('home.html')


def app_profile():
    return render_template('profile/profile.html')


def app_register():
    if request.method == "GET":
        return render_template('auth/register.html')
    elif request.method == "POST":
        db = get_db()
        error = None
        username = request.form["username"]
        email = request.form["email"]
        password = request.form["password"]
        password_confirm = request.form["password_confirm"]

        if not username:
            error = "Usernam is required."
        elif not password:
            error = "Password is required."
        elif not password_confirm:
            error = "Password confirmation is required."
        elif not email:
            error = "Email is required."
        elif db.execute("SELECT * FROM users WHERE username = ?", (username,)).fetchone() is not None:
            error = "User {} is already registered.".format(username)
        elif password_confirm != password:
            error = "The password fields must be equal"

        if error is None:
            hashed_password = generate_password_hash(password)
            db.execute("INSERT INTO users(username, email, password) VALUES(?, ?, ?)",
                       (username, email, hashed_password,))
            db.commit()
            return redirect(url_for("login"))
        flash(error)
        return render_template("authregister.html", error=error)


def app_login():
    if request.method == "GET":
        return render_template('auth/login.html')
    elif request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]
        db = get_db()
        error = None
        if not username:
            error = "Username is required."
        elif not password:
            error = "Password is required"
        user = db.execute(
            "SELECT * FROM users WHERE username = ?", (username,)).fetchone()

        if user is None:
            error = "Incorrect username."
        elif not check_password_hash(user["password"], password):
            error = "Incorrect password."

        if error is None:
            session.clear()
            session["user_id"] = user["id"]
            session["username"] = user["username"]
            session["email"] = user["email"]
            return redirect(url_for('profile'))


def app_logout():
    session.clear()
    return redirect(url_for("login"))


def app_load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = get_db().execute(
            'SELECT * FROM users WHERE id = ?', (user_id,)
        ).fetchone()


def app_handle_user(id):
    db = get_db()
    if request.method == "GET":
        user = db.execute("SELECT * FROM users WHERE id = ?",
                          (id,)).fetchone()
        db.commit()
        if user is not None:
            return jsonify(id=user["id"], username=user["username"], email=user["email"], password=user["password"])
        else:
            return "User with the id {id} hasn't been found".format(id)
    elif request.method == "PUT":
        username = request.form["username"]
        email = request.form["email"]
        password = request.form["password"]
        update_user = db.execute("UPDATE users SET username = ?, email=?, password=? WHERE id=?",
                                 (username, email, generate_password_hash(password), id))
        db.commit()
        if update_user is not None:
            return "User updated"
        else:
            return "User with this id doesn't exist"
    elif request.method == "DELETE":
        user_to_delete = db.execute("DELETE FROM users WHERE id = ?", (id, ))
        db.commit()
        if user_to_delete is not None:
            return "User deleted"
        else:
            return "This user doesn't exist in the database"


def app_get_users():
    db = get_db()
    users = db.execute("SELECT * from users").fetchall()
    db.commit()

    if users is not None:
        all_the_users = []
        for user in users:
            all_the_users.append(
                {
                    "id": user["id"],
                    "username": user["username"],
                    "email": user["email"],
                    "password": user["password"]
                }
            )
        return jsonify(all_the_users)
    else:
        return "An error occured during the acces to the database"
