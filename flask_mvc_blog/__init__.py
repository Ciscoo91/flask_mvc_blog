import os
from .Controllers import usersController
from .Controllers import articlesController
from flask import Flask, Blueprint
from flask_mvc_blog.Config.db import close_db, initdb_command

app = Flask(__name__)
app.config.from_object(__name__)
app.config.from_envvar("FLASKR_SETTINGS", silent=True)
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, "flaskr.db"),
    SECRET_KEY="development_key",
    USERNAME="admin",
    PASSWORD="default"
))


@app.cli.command('initdb')
def app_initdb_command():
    initdb_command(app)


@app.teardown_appcontext
def app_close_db(arg):
    close_db(arg)
